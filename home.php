<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="AT&T Business">
  <link rel="shortcut icon" type='image/x-icon' href="./public/img/favicon.png">
  <title>AT&T Business</title>
  <link rel="stylesheet" type="text/css" href="./src/css/home.css"/>
  <link rel="stylesheet" type="text/css" href="./src/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="./src/css/_main.css"/>
</head>

<body>
<!-- HEADER -->
<?php include './header.php';?>
<!-- END HEADER -->
<!-- CONTENT -->
<div id="main-body">
  <section id="logged-section" class="flex">
    <div id="menu">
      <div id="close-menu-mobile"><span>X</span></div>
      <div class="menu-col" id="resumen">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-resumen.png"><div class="menu-icon-title">RESUMEN</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen</a></div>
          <div class="menu-item-sub-item"><a href="#">Análisis</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-pagos.png"><div class="menu-icon-title">PAGOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen de Factura</a></div>
          <div class="menu-item-sub-item"><a href="#">Completar Pago</a></div>
          <div class="menu-item-sub-item"><a href="#">Historial de Pagos</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-mis_servicios.png"><div class="menu-icon-title">MIS SERVICIOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Servicios Activos</a></div>
          <div class="menu-item-sub-item"><a href="#">Ordenes</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-ayuda_tecnica.png"><div class="menu-icon-title">AYUDA TÉCNICA</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Nuevo Ticket</a></div>
          <div class="menu-item-sub-item"><a href="#">Resumen de tickets</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-perfil.png"><div class="menu-icon-title">PERFIL</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Manage Profile</a></div>
        </div>
      </div>

    </div>
    <div id="right">
      <div class="resumen-main-box">

        <div class="resumen-box1 mr-20">
          <div class="resumen-box1-main-left">
            <div class="resumen-box1-left">
              <div id="resumen-value-1" class="copy-center font-large bold font-gray">$000000.00</div>
            </div>
            <div class="resumen-box1-middle">
              <div class="copy-center-middle">
                <div class="font-small bold">Seleccionar periodo</div>
                <div class="font-small bold font-gray">Estado actual</div>
                <div id="resumen-value-date-1" class="font-small bold font-gray">Aug 19 - Sep 18</div>
              </div>
            </div>
            <div class="resumen-box1-main-left-desplegate bold">
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.02</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 15 - Sep 13</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.03</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 14 - Sep 12</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.04</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 16 - Sep 11</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.05</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 17 - Sep 10</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.06</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 18 - Sep 09</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.07</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 19 - Sep 08</span></div></div>
            </div>
          </div>
          <div class="resumen-box1-right" id="desplegate-sub-menu-icon-2"></div>
        </div>

        <a href="#" class="resumen-box2 bold">Ver periodos pasados</a>

      </div>
      <div class="box border-blue">
          <div id="resumen-factura" class="box-2 mt-20 mb-20">
              <div class="hd1 ml-20">Resumen de la factura</div>
              <div class="flex justify">
                  <div class="box-2-left ml-20">
                      <div class="row-resumen">
                          <div class="row-resumen-left">Balance anterior: </div>
                          <div class="row-resumen-dots-horizontal">................................................................................................................. </div>
                          <div class="row-resumen-right">$40.00</div>
                      </div>
                      <div class="row-resumen">
                          <div class="row-resumen-left">Pagos recibidos: </div>
                          <div class="row-resumen-dots-horizontal">............................................................................................................ </div>
                          <div class="row-resumen-right">$853.82</div>
                      </div>
                      <div class="row-resumen">
                          <div class="row-resumen-left">Cargos corrientes: </div>
                          <div class="row-resumen-dots-horizontal">............................................................................................................ </div>
                          <div class="row-resumen-right">$0.00</div>
                      </div>
                      <div class="row-resumen">
                          <div class="row-resumen-left">Cantidad adeudada: </div>
                          <div class="row-resumen-dots-horizontal">............................................................................................................ </div>
                          <div class="row-resumen-right">$80.00</div>
                      </div>
                      <div class="row-resumen">
                          <div class="row-resumen-left">Cantidad vencida: </div>
                          <div class="row-resumen-dots-horizontal">............................................................................... </div>
                          <div class="row-resumen-right">$853.82</div>
                      </div>
                  </div>
                  <div class="box-2-right">
                      <div class="complete_payment_summary_bill">Completar un pago</div>
                      <div class="btn-white">Ver factura completa</div>
                      <div class="btn-white">Ver todos los cargos</div>
                      <div class="btn-white">Ver planes y resumen de servicios</div>
                  </div>
              </div>
          </div>
      </div>
      <div class="access_title">Accede a:</div>
      <div class="container_block">
          <div class="block_selected">
              <div class="text-center circle"><div class="dollar bold">$</div></div>
              <div class="text-center option_title">pagos</div>
          </div>
          <div class="block">
              <img class="service_icon" src="src/drawable/service_icon_blue.png" width="22%">
              <div class="text-center service_title">mis servicios</div>
          </div>
          <div class="block">
              <img class="technical_help_icon" src="src/drawable/technical_help_icon_blue.png" width="45%">
              <div class="text-center technical_help_title">ayuda técnica</div>
          </div>
          <div class="block">
              <img class="profile_icon" src="src/drawable/profile_icon_blue.png" width="45%">
              <div class="text-center profile_title">perfil</div>
          </div>
      </div>
    </div>
  </section>
</div>
<!-- END CONTENT -->
<!-- FOOTER -->
<footer id="main-footer">
  <div class="footer-center">
    <div class="footer-top flex justify mb-40">
      <div>
        <div><a class="footer_options bold">Make a Payment</a></div>
        <div><a class="footer_options bold">Manage Profile</a></div>
        <div><a class="footer_options bold">Frequently Asked Questions</a></div>
      </div>
      <div>
        <div><a class="footer_options">Contact AT&T Business</a></div>
        <div><a class="footer_options">Help & Support</a></div>
      </div>
      <div>
        <div class="find_us">FIND US</div>
        <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
        <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
      <div class="align-center">
        <span><a href="#" class="footer_options">Términos de uso</a> | </span>
        <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
        <span><a href="#" class="footer_options">Accesibilidad</a></span>
      </div>
    </div>
  </div>
</footer>
<script src="./src/js/jquery-3.2.1.min.js"></script>
<script src="./src/js/App.js"></script>
</body>
</html>
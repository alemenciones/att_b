<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="ATT BUSINESS">
    <!-- <base href="http://localhost/att_business/"> -->

    <link type="text/css" rel="stylesheet" href="src/css/login.css"/>
    <link type="text/css" rel="stylesheet" href="src/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="./src/css/_main.css"/>
    <title>Att Business</title>
</head>
<body>

    <!-- start header -->
    <!-- <div class="row header">
        <img class="logo" src="src/drawable/att-logo.png" width="3.5%">
        <span class="logo_title_att">AT&T</span>
        <span class="logo_title_business">Business</span>
        <input class="language_selected" type="button" id="espanol" value="Español">
        <input class="language_option" type="button" id="espanol" value="English">
    </div> -->
    <header id="main-header">
      <div class="main-header-center">
        <div class="logo">
          <img class="att-logo" src="./src/drawable/att-logo.png">
          <div class="att-logo-copy"><span style="font-family: ATTAleckSans-Regular">AT&T</span> Business</div>
        </div>
        <div class="main-header-language-box">
          <a href="#" class="main-header-language-box-option-active">Español</a>
          <a href="#" class="main-header-language-box-option">English</a>
        </div>
      </div>
    </header>

    <!-- end header -->

    <!-- Componente para mostrar mensaje de login -->
    <div id="message" class="row text-center message_container" style=""></div>

    <div class="row bg_login">
        <div class="col-sm-3 col-sm-offset-2 login_panel"  style="margin-right: 3%">
            <form>
                <div style="margin-bottom: 15%">
                    <h1 class="login_title">Accede</h1>
                    <h3 class="login_sub_title">a tu servicio de </h3>
                    <h3 class="login_sub_title">línea fija</h3>
                </div>
                <!-- Componente para mostrar los requerimientos del password -->
                <div id="password_requirements"></div>

                <div class="form-group" >
                    <label id="lbl_user" class="label_component" for="user">Usuario:</label>
                    <div class="input_error" id="input_error_user">
                        <input class="input_component" type="text" id="form_user">
                    </div>
                </div>
                <div class="form-group">
                    <label id="lbl_password" class="label_component" for="password">Contraseña:</label>
                    <div class="input_error" id="input_error_password">
                        <input class="input_component" type="text" id="form_password">
                    </div>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn_login">ingresar</button>
                </div>
                <input type="checkbox" name="save_user_id"><span class="chbx_text">Guardar ID del usuario</span>
                <p class="chbx_text">Indica si tu conexión es:</p>
                <input type="checkbox" name="public"><span class="conexion_type">Pública</span>
                <input type="checkbox" name="private"><span class="conexion_type">Privada</span>
            </form>
            <div class="row" style="margin-top: 15%; margin-bottom: 10.6%">
                <div class="col-sm-12"><a class="forgot_password"><u>¿Olvidaste tu contraseña?</u></a></div>
                <div class="col-sm-12"><a class="forgot_password"><u>Preguntas frecuentes</u></a></div>
            </div>
        </div>
        <div class="col-sm-5 login_panel">
            <p class="rules_title">Este portal te da la oportunidad de ver y manejar las cuentas que tienes con AT&T.</p>
            <p class="transactions">Las transascciones disponibles en este portal son:</p>
            <ul>
                <li class="transaction_option">Pagar tu factura</li>
                <li class="transaction_option">Ver el resumen e historial de tu cuenta</li>
                <li class="transaction_option">Ver tus facturas y estatus de órdenes</li>
                <li class="transaction_option">Bajar archivos</li>
                <li class="transaction_option">Contactarnos</li>
            </ul>
            <p class="more_information">Para información más detallada sobre las transacciones disponibles, visita la sección de <a class="link_faq"><u>preguntas frecuentes</u></a>.</p>
        </div>
    </div>

    <!-- start footer -->
    <footer id="main-footer">
      <div class="footer-center">
        <div class="footer-top flex justify mb-40">
          <div>
            <div><a class="footer_options bold">Make a Payment</a></div>
            <div><a class="footer_options bold">Manage Profile</a></div>
            <div><a class="footer_options bold">Frequently Asked Questions</a></div>
          </div>
          <div>
            <div><a class="footer_options">Contact AT&T Business</a></div>
            <div><a class="footer_options">Help & Support</a></div>
          </div>
          <div>
            <div class="find_us">FIND US</div>
            <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
            <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
          </div>
        </div>
        <div class="footer-bottom">
          <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
          <div class="align-center">
            <span><a href="#" class="footer_options">Términos de uso</a> | </span>
            <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
            <span><a href="#" class="footer_options">Accesibilidad</a></span>
          </div>
        </div>
      </div>
    </footer>
<!--     <div class="row footer_bg">
        <div class="col-sm-6 col-sm-offset-3 footer">
            <div class="col-sm-4">
                <div><a class="footer_options">Make a Payment</a></div>
                <div><a class="footer_options">Manage Profile</a></div>
                <div><a class="footer_options">Frequently Asked Questions</a></div>
            </div>
            <div class="col-sm-4 footer_center">
                <div><a class="footer_options">Contact AT&T Business</a></div>
                <div><a class="footer_options">Help & Support</a></div>
            </div>
            <div class="col-sm-4">
                <div class="find_us">FIND US</div>
                <a href=""><img src="src/drawable/fb_logo.png" width="16%"></a>
                <a href=""><img src="src/drawable/ins_logo.png" width="16%"></a>
            </div>
        </div>
        <div class="col-sm-12 text-center rights_reserved">
            <p class="">@2017. All rights reserved. AT&T Puerto Rico.</p>
            <span><a class="footer_options">Términos de uso</a> | </span>
            <span><a class="footer_options">Política de Privacidad</a> | </span>
            <span><a class="footer_options">Accesibilidad</a></span>
        </div>
    </div> -->
    <!-- end footer -->

    <script type="text/javascript" src="src/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="src/js/login.js"></script>
    <script>
        $(document).ready(function(){
            var elements = ['user', 'password'];
            login.validateLogin(1, elements, 'El usuario y/o la contraseña no son correctos');
        });
    </script>
</body>
</html>
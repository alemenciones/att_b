<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="ATT BUSINESS">
    <!-- <base href="http://localhost/att_business/"> -->
    <link type="text/css" rel="stylesheet" href="src/css/faq.css"/>
    <link type="text/css" rel="stylesheet" href="src/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="./src/css/_main.css"/>

    <title>Att Business</title>
</head>
<body>
    <!-- start faq header -->
    <div class="row text-center" style="background-image: url('src/drawable/bg_faq.png');">
        <h1 class="faq_title">Preguntas frecuentes</h1>
        <h3 class="faq_sub_title">Introduzca palabra (s) clave (s) para su búsqueda.</h3>
        <div class="col-sm-6 col-sm-offset-3">
            <form>
                <input type="text" class="form-control input_search" id="form_search" placeholder="búsqueda">
                <img class="search_icon" src="src/drawable/search_icon.png" width="3%">
                <button type="submit" class="btn_search">buscar</button>
            </form>
        </div>
    </div>
    <!-- end faq header -->
    <div class="row">
        <div class="col-sm-2 col-sm-offset-1 faq_summary">
            <p class="faq_topic">Mi cuenta</p>
            <p class="faq_topic">Cuenta de comercial</p>
            <p class="faq_topic">Cuenta de línea fija</p>
            <p class="faq_topic">Funcionalidades de mi cuenta</p>
        </div>
        <div class="col-sm-8 faq_content">
            <h1 class="option_title">Mi cuenta</h1>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Qué es My Account?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#my_account">
                </div>
                <div class="collapse in panel-body panel_text" id="my_account">My Account es la aplicación para clientes de negocios que le permitirá acceder a su cuenta de servicios de línea fija.</div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Qué transacciones puedo hacer en My Account?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#transactions">
                </div>
                <div class="collapse in panel-body panel_text" id="transactions">
                    <ul>
                        <li>ver el resumen financiero</li>
                        <li>pagar su cuenta con Visa, Master Card, American Express, cuenta de cheque o débito</li>
                        <li>ver historial de cuenta</li>
                        <li>ver su factura</li>
                        <li>ver servicios activos</li>
                        <li>ver status de órdenes</li>
                        <li>ver estatus de los tickets de apoyo técnico</li>
                        <li>descargar archivos</li>
                        <li>manejo de su perfil</li>
                        <li>contactarnos</li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Cómo se activa My Account?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#activate_my_account">
                </div>
                <div class="collapse in panel-body panel_text" id="activate_my_account">
                    <ul>
                        <li>Cliente nuevo va a estar activo automáticamente, usted recibirá un correo de bienvenida con sus credenciales para acceder a la aplicación.</li>
                        <li>Cliente existente que desee realizar cambio, puede llamar al centro de llamadas al 787-717-9700 o ir a My Account y realizar cambios en el perfil llenando el Customer Information Form.</li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Cuántos usuarios puedo tener activos en "My Account"?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#active_users">
                </div>
                <div class="collapse in panel-body panel_text" id="active_users">
                    <ul><li>Usted puede tener la cantidad de usuarios que desee</li></ul>
                </div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Cómo añado o elimino usuarios de "My Account"?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#delete_user">
                </div>
                <div class="collapse in panel-body panel_text" id="delete_user">
                    <ul><li>Cliente existente que desee realizar cambio, puede llamar al centro de llamadas al 787-717-9700 o ir a My Account y realizar cambios en el perfil llenando el Customer Information Form.</li></ul>
                </div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Cuánto tiempo tarda la activación de My Account?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#time_activation">
                </div>
                <div class="collapse in panel-body panel_text" id="time_activation">
                    <ul>
                        <li>La creación del usuario podrá tomar 24 horas. Una vez creado, el cliente recibirá un email de bienvenida con sus credenciales.</li>
                        <li>Si es una nueva cuenta el cliente no verá su última factura hasta el siguiente ciclo de facturación.</li>
                        <li>Si es un cliente existente con un nuevo usuario, la última factura estará disponible.</li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Cuáles son los servicios activos que podré ver a través de My Account?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#active_services">
                </div>
                <div class="collapse in panel-body panel_text" id="active_services">Abajo un listado de servicios disponibles en My Account.
                    <p><strong>Servicios de telefonía:</strong></p>
                    <ul>
                        <li>POTS</li>
                        <li>DS1</li>
                        <li>PRI</li>
                        <li>Toll Free</li>
                    </ul>
                    <p><strong>Servicios de Data:</strong></p>
                    <ul>
                        <li>Líneas privadas</li>
                        <li>Ethernet</li>
                        <li>Internet</li>
                        <li>Data Center</li>
                    </ul>
                    <p><strong>Servicios VoIP:</strong></p>
                    <ul>
                        <li>Aptus Hosted</li>
                        <li>SIP Trunk</li>
                        <li>Business Trunks</li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Cuál es el costo de My Account?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#my_account_cost">
                </div>
                <div class="collapse in panel-body panel_text" id="my_account_cost">
                    <ul>
                        <li>My Account es un servicio gratuito para nuestros clientes. Hay tres opciones disponibles: imagen PDF, imagen PDF con detalle de llamadas a larga distancia. Si el cliente desea una base de datos Microsoft Access que incluye todo el detalle de llamadas debe llamar para solicitarlo.</li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Cómo reportar problemas con "My Account"?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#my_account_problem">
                </div>
                <div class="collapse in panel-body panel_text" id="my_account_problem">Contáctenos
                    <ul>
                        <li>Llámenos al (787) 717 9700</li>
                        <li>Email: </li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default faq_panel">
                <div class="panel-heading panel_title">¿Olvidó su contraseña?
                    <img class="more" src="src/drawable/sign_more.png" width="3%" data-toggle="collapse" data-target="#forgot_password">
                </div>
                <div class="collapse in panel-body panel_text" id="forgot_password">Siga los pasos en la sección de "Password reset"
                </div>
            </div>
        </div>
    </div>

    <footer id="main-footer">
      <div class="footer-center">
        <div class="footer-top flex justify mb-40">
          <div>
            <div><a class="footer_options bold">Make a Payment</a></div>
            <div><a class="footer_options bold">Manage Profile</a></div>
            <div><a class="footer_options bold">Frequently Asked Questions</a></div>
          </div>
          <div>
            <div><a class="footer_options">Contact AT&T Business</a></div>
            <div><a class="footer_options">Help & Support</a></div>
          </div>
          <div>
            <div class="find_us">FIND US</div>
            <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
            <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
          </div>
        </div>
        <div class="footer-bottom">
          <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
          <div class="align-center">
            <span><a href="#" class="footer_options">Términos de uso</a> | </span>
            <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
            <span><a href="#" class="footer_options">Accesibilidad</a></span>
          </div>
        </div>
      </div>
    </footer>

    <script type="text/javascript" src="src/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="src/js/app/login.js"></script> -->
    <script>
        $(document).ready(function(){
            // login.validateLogin(1, 'user', 'El usuario y/o la contraseña no son correctos');
        });
    </script>
</body>
</html>
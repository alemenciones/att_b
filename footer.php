<div class="row footer_bg">
    <div class="col-sm-6 col-sm-offset-3 footer">
        <div class="col-sm-4">
            <div><a class="footer_options">Make a Payment</a></div>
            <div><a class="footer_options">Manage Profile</a></div>
            <div><a class="footer_options">Frequently Asked Questions</a></div>
        </div>
        <div class="col-sm-4 footer_center">
            <div><a class="footer_options">Contact AT&T Business</a></div>
            <div><a class="footer_options">Help & Support</a></div>
        </div>
        <div class="col-sm-4">
            <div class="find_us">FIND US</div>
            <a href=""><img src="src/drawable/fb_logo.png" width="15%"></a>
            <a href=""><img src="src/drawable/ins_logo.png" width="15%"></a>
        </div>
    </div>
    <div class="col-sm-12 text-center rights_reserved mt-40">
        <p class="">@2017. All rights reserved. AT&T Puerto Rico.</p>
        <span><a class="footer_options">Términos de uso</a> | </span>
        <span><a class="footer_options">Política de Privacidad</a> | </span>
        <span><a class="footer_options">Accesibilidad</a></span>
    </div>
</div>
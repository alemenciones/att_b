<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="AT&T Business">
  <link rel="shortcut icon" type='image/x-icon' href="./public/img/favicon.png">
  <title>AT&T Business</title>
  <link rel="stylesheet" type="text/css" href="./src/css/_main.css" />
  <link rel="stylesheet" type="text/css" href="./src/css/logged.css" />
</head>

<body>
<!-- HEADER -->
<header id="main-header">
  <div class="main-header-center">
    <div class="logo">
      <img class="att-logo" src="./src/drawable/att-logo.png">
      <div class="att-logo-copy"><span style="font-family: Arial">AT&T</span> Business</div>
    </div>
    <div class="main-header-language-box">
      <a href="#" class="main-header-language-box-option-active">Español</a>
      <a href="#" class="main-header-language-box-option">English</a>
    </div>
  </div>
</header>
<div id="sub-menu">
  <div id="sub-menu-center">

    <div class="sub-menu-col">
      <div class="font-large medium mt-10">Bienvenida, Karla</div>
      <div class="font-small"><span class="bold">Ultima visíta:</span> 10/13/2017 <span class="ml-10 font-blue">4:25pm</span></div>
    </div>

    <div class="sub-menu-col sub-menu-col-gray">
      <div class="sub-menu-2-left">
        <img class="sub-menu-2-left-img" src="./src/drawable/icon-profile.png">
        <div class="sub-menu-2-left-right ml-10">
          <div class="font-small">cuenta</div>
          <div id="desplegate-value" class=" font-blue font-large bold">000000-0</div>
        </div>
        <div class="sub-menu-2-left-desplegate">
          <div class="desplegate-item">000000-1</div>
          <div class="desplegate-item">000000-2</div>
          <div class="desplegate-item">000000-3</div>
          <div class="desplegate-item">000000-4</div>
          <div class="desplegate-item">000000-5</div>
        </div>
      </div>
      <div id="desplegate-sub-menu-icon" class="sub-menu-2-right"></div>
    </div>

    <div class="sub-menu-col sub-menu-col-gray">
      <div class="sub-menu-2-left-2">
        <img class="sub-menu-2-left-img" src="./src/drawable/icon-dollar.png">
        <div class="sub-menu-2-left-right ml-10 mr-10">
          <div class="font-small">Balance actual</div>
          <div class=" font-blue font-large bold">$853.82</div>
        </div>
      </div>
    </div>

    <a href="#" class="sub-menu-col complete-payment ml-30 bold">COMPLETAR UN PAGO</a>

    <div class="sub-menu-close-session">
      <a href="#">cerrar sesion</a> <img class="icon-logout" src="./src/drawable/icon-logout.png">
    </div>

  </div>
</div>
<!-- END HEADER -->
<!-- CONTENT -->
<div id="main-body">
  <section id="logged-section" class="flex">
    <div id="menu">

      <div class="menu-col" id="resumen">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-resumen.png"><div class="menu-icon-title">RESUMEN</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen</a></div>
          <div class="menu-item-sub-item"><a href="#">Análisis</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-pagos.png"><div class="menu-icon-title">PAGOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen de Factura</a></div>
          <div class="menu-item-sub-item"><a href="#">Completar Pago</a></div>
          <div class="menu-item-sub-item"><a href="#">Historial de Pagos</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-mis_servicios.png"><div class="menu-icon-title">MIS SERVICIOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Servicios Activos</a></div>
          <div class="menu-item-sub-item"><a href="#">Ordenes</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-ayuda_tecnica.png"><div class="menu-icon-title">AYUDA TÉCNICA</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Nuevo Ticket</a></div>
          <div class="menu-item-sub-item"><a href="#">Resumen de tickets</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-perfil.png"><div class="menu-icon-title">PERFIL</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Manage Profile</a></div>
        </div>
      </div>

    </div>
    <div id="right">
      <div class="resumen-main-box">

        <div class="resumen-box1 mr-20">
          <div class="resumen-box1-main-left">
            <div class="resumen-box1-left">
              <div id="resumen-value-1" class="copy-center font-large bold font-gray">$000000.00</div>
            </div>
            <div class="resumen-box1-middle">
              <div class="copy-center-middle">
                <div class="font-small bold">Seleccionar periodo</div>
                <div class="font-small bold font-gray">Estado actual</div>
                <div id="resumen-value-date-1" class="font-small bold font-gray">Aug 19 - Sep 18</div>
              </div>
            </div>
            <div class="resumen-box1-main-left-desplegate bold">
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.02</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 15 - Sep 13</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.03</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 14 - Sep 12</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.04</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 16 - Sep 11</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.05</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 17 - Sep 10</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.06</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 18 - Sep 09</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.07</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 19 - Sep 08</span></div></div>
            </div>
          </div>
          <div class="resumen-box1-right" id="desplegate-sub-menu-icon-2"></div>
        </div>

        <a href="#" class="resumen-box2 bold">Ver periodos pasados</a>

      </div>
      <div class="box border-blue">asd</div>
    </div>
  </section>
</div>
<!-- END CONTENT -->
<!-- FOOTER -->
<footer id="main-footer">
  <div class="footer-center">
    <div class="footer-top flex justify mb-40">
      <div>
        <div><a class="footer_options bold">Make a Payment</a></div>
        <div><a class="footer_options bold">Manage Profile</a></div>
        <div><a class="footer_options bold">Frequently Asked Questions</a></div>
      </div>
      <div>
        <div><a class="footer_options">Contact AT&T Business</a></div>
        <div><a class="footer_options">Help & Support</a></div>
      </div>
      <div>
        <div class="find_us">FIND US</div>
        <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
        <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
      <div class="align-center">
        <span><a href="#" class="footer_options">Términos de uso</a> | </span>
        <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
        <span><a href="#" class="footer_options">Accesibilidad</a></span>
      </div>
    </div>
  </div>
</footer>
<script src="./src/js/jquery-3.2.1.min.js"></script>
<script src="./src/js/App.js"></script>
</body>
</html>
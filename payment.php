<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="AT&T Business">
  <link rel="shortcut icon" type='image/x-icon' href="./public/img/favicon.png">
  <title>AT&T Business</title>
  <link rel="stylesheet" type="text/css" href="./src/css/payment.css"/>
  <link rel="stylesheet" type="text/css" href="./src/css/home.css"/>
  <link rel="stylesheet" type="text/css" href="./src/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="./src/css/_main.css"/>
</head>

<body>
<!-- HEADER -->
<?php include './header.php';?>
<!-- END HEADER -->
<!-- CONTENT -->
<div id="main-body">
  <section id="logged-section" class="flex">
    <div id="menu">
      <div id="close-menu-mobile"><span>X</span></div>
      <div class="menu-col" id="resumen">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-resumen.png"><div class="menu-icon-title">RESUMEN</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen</a></div>
          <div class="menu-item-sub-item"><a href="#">Análisis</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-pagos.png"><div class="menu-icon-title">PAGOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen de Factura</a></div>
          <div class="menu-item-sub-item"><a href="#">Completar Pago</a></div>
          <div class="menu-item-sub-item"><a href="#">Historial de Pagos</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-mis_servicios.png"><div class="menu-icon-title">MIS SERVICIOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Servicios Activos</a></div>
          <div class="menu-item-sub-item"><a href="#">Ordenes</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-ayuda_tecnica.png"><div class="menu-icon-title">AYUDA TÉCNICA</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Nuevo Ticket</a></div>
          <div class="menu-item-sub-item"><a href="#">Resumen de tickets</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-perfil.png"><div class="menu-icon-title">PERFIL</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Manage Profile</a></div>
        </div>
      </div>

    </div>
    <div id="right">
      <div class="resumen-main-box">
        <div class="resumen-box1 mr-20">
          <div class="resumen-box1-main-left">
            <div class="resumen-box1-left">
              <div id="resumen-value-1" class="copy-center font-large bold font-gray">$000000.00</div>
            </div>
            <div class="resumen-box1-middle">
              <div class="copy-center-middle">
                <div class="font-small bold">Seleccionar periodo</div>
                <div class="font-small bold font-gray">Estado actual</div>
                <div id="resumen-value-date-1" class="font-small bold font-gray">Aug 19 - Sep 18</div>
              </div>
            </div>
            <div class="resumen-box1-main-left-desplegate bold">
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.02</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 15 - Sep 13</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.03</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 14 - Sep 12</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.04</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 16 - Sep 11</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.05</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 17 - Sep 10</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.06</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 18 - Sep 09</span></div></div>
              <div class="resumen-desplegate-item"><span class="resumen-value-number">$000000.07</span> <div class="resumen-desplegate-item-right font-small">Estado actual <span class="resumen-desplegate-item-right-copy">Aug 19 - Sep 08</span></div></div>
            </div>
          </div>
          <div class="resumen-box1-right" id="desplegate-sub-menu-icon-2"></div>
        </div>

        <a href="#" class="resumen-box2 bold">Ver periodos pasados</a>
      </div>

      <div class="box border-blue">
          <div id="resumen-factura" class="box-2 mt-20 mb-20">
              <div class="hd1 ml-20">Resumen de la factura</div>
              <div class="flex justify">
                  <div class="box-2-left ml-20">
                      <div class="row-resumen">
                          <div class="row-resumen-left">Balance anterior: </div>
                          <div class="row-resumen-dots-horizontal">................................................................................................................. </div>
                          <div class="row-resumen-right">$40.00</div>
                      </div>
                      <div class="row-resumen">
                          <div class="row-resumen-left">Pagos recibidos: </div>
                          <div class="row-resumen-dots-horizontal">............................................................................................................ </div>
                          <div class="row-resumen-right">$853.82</div>
                      </div>
                      <div class="row-resumen">
                          <div class="row-resumen-left">Cargos corrientes: </div>
                          <div class="row-resumen-dots-horizontal">............................................................................................................ </div>
                          <div class="row-resumen-right">$0.00</div>
                      </div>
                      <div class="row-resumen">
                          <div class="row-resumen-left">Cantidad adeudada: </div>
                          <div class="row-resumen-dots-horizontal">............................................................................................................ </div>
                          <div class="row-resumen-right">$80.00</div>
                      </div>
                      <div class="row-resumen">
                          <div class="row-resumen-left">Cantidad vencida: </div>
                          <div class="row-resumen-dots-horizontal">............................................................................... </div>
                          <div class="row-resumen-right">$853.82</div>
                      </div>
                  </div>
                  <div class="box-2-right">
                      <div class="complete_payment_summary_bill">Completar un pago</div>
                      <div class="btn-white">Ver factura completa</div>
                      <div class="btn-white">Ver todos los cargos</div>
                      <div class="btn-white">Ver planes y resumen de servicios</div>
                  </div>
              </div>
          </div>
      </div>

      <!-- start step 1 -->
      <div class="box complete-payment-steps">
        <div class="ml-20 flex">
          <div class="circle-step circle-active"><div class="circle-active-text bold">1</div></div>
          <div class="choose-amount-title">Escoja la cantidad a pagar:</div>
        </div>

        <div class="flex account-data">
          <div class="input-component text-right">
              <label style="" for="account_holder">Titular de la cuenta:</label>
              <input type="text" class="form-control disable" id="account_holder" name="account_holder">
          </div>
          <div class="input-component" style="margin-left: 2%;">
              <label class="text-center" for="account_number">Número de cuenta:</label>
              <input type="text" class="form-control disable" id="account_number" name="account_number">
          </div>
        </div>

        <div class="step2-form-container">
            <div class="m-row">
                <div class="m-row-left">Cantidad:</div>
                <div class="m-row-right"><input class="form-control input-short disable" type="text" value="$895.00"></div>
                <div class="circle-amount circle-amount-active"></div>
            </div>
            <div class="m-row">
                <div class="m-row-left">Cantidad adeudada:</div>
                <div class="m-row-right"><input class="form-control input-short disable" type="text" value="$1200.00"></div>
                <div class="circle-amount circle-amount-green"></div>
            </div>
            <div class="m-row another-amount">
                <div class="m-row-left">Otra cantidad:</div>
                <div class="m-row-right"><input class="form-control input-short" type="text" value="$000.00"></div>
            </div>
        </div>

        <div class="text-center complete-payment-steps-footer">
          <button class="btn-continue">continuar</button>
        </div>
      </div>
      <!-- end step 1 -->

      <!-- start step 2 -->
      <div class="box complete-payment-steps">
        <div class="ml-20 flex">
          <div class="circle-step circle-active"><div class="circle-active-text bold">2</div></div>
          <div class="choose-amount-title">Pagar:</div>
        </div>

        <div class="redirect-evertec-container">
          <div class="redirect-evertec text-center">Redirigiendo a la página de pago Evertec</div>
          <div class="arrow-progress-container">
            <div class="redirect-progress">
              <div class="arrow-progress">
                  <div class="arrow"></div>
                  <div class="triangulo right">
                    <div></div>
                  </div>
              </div>
            </div>
          </div>
        </div>

        <div class="text-center complete-payment-steps-footer">
          <button class="btn-continue">continuar</button>
        </div>
      </div>
      <!-- end step 2 -->

      <!-- start step 3 -->
      <div class="box complete-payment-steps">
        <div class="ml-20 flex">
          <div class="circle-step circle-active"><div class="circle-active-text bold">3</div></div>
          <div class="choose-amount-title">Confirmación de pago:</div>
        </div>

        <div class="step3-form-container">Pendiente a confirmación al API de EVERTEC</div>
        <div class="step3-form-container">Número de confirmación:<br/>281374939jfomx</div>
      </div>
      <!-- end step 3 -->

      <div class="services-title1">Historial de pagos</div>

      <div class="payment-history">
          <div class="recovery_password-footer text-right">
              <button class="btn-download-bill-summary-active">Download bill summary</button>
              <button class="btn-download-bill-summary">Download Bill Analyze</button>
          </div>
          <table class="table table-responsive table-hover">
              <thead>
                  <tr>
                      <th>Factura</th>
                      <th>Pagos</th>
                      <th>Ajustes</th>
                      <th>Historial completo</th>
                  </tr>
              </thead>
              <tbody>
                  <tr >
                      <td>INV0000</td>
                      <td>$150.00</td>
                      <td>$50.00</td>
                      <td>$100.00</td>
                  </tr>
                  <tr>
                      <td>INV0000</td>
                      <td>$150.00</td>
                      <td>$50.00</td>
                      <td>$100.00</td>
                  </tr>
                  <tr>
                      <td>INV0000</td>
                      <td>$150.00</td>
                      <td>$50.00</td>
                      <td>$100.00</td>
                  </tr>
                  <tr>
                      <td>INV0000</td>
                      <td>$150.00</td>
                      <td>$50.00</td>
                      <td>$100.00</td>
                  </tr>
                  <tr>
                      <td>INV0000</td>
                      <td>$150.00</td>
                      <td>$50.00</td>
                      <td>$100.00</td>
                  </tr>
              </tbody>
          </table>
      </div>
    </div>
  </section>
</div>
<!-- END CONTENT -->
<!-- FOOTER -->
<footer id="main-footer">
  <div class="footer-center">
    <div class="footer-top flex justify mb-40">
      <div>
        <div><a class="footer_options bold">Make a Payment</a></div>
        <div><a class="footer_options bold">Manage Profile</a></div>
        <div><a class="footer_options bold">Frequently Asked Questions</a></div>
      </div>
      <div>
        <div><a class="footer_options">Contact AT&T Business</a></div>
        <div><a class="footer_options">Help & Support</a></div>
      </div>
      <div>
        <div class="find_us">FIND US</div>
        <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
        <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
      <div class="align-center">
        <span><a href="#" class="footer_options">Términos de uso</a> | </span>
        <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
        <span><a href="#" class="footer_options">Accesibilidad</a></span>
      </div>
    </div>
  </div>
</footer>
<script src="./src/js/jquery-3.2.1.min.js"></script>
<script src="./src/js/App.js"></script>
<script>
    $(document).ready(function(){
        $.ajax({
          xhr: function () {
              var xhr = new window.XMLHttpRequest();
              xhr.addEventListener("progress", function (evt) {
                  if (evt.lengthComputable) {
                      var percentComplete = evt.loaded / evt.total;
                      console.log(percentComplete);

                        $('.redirect-progress').animate({
                          width: percentComplete * 100 + '%'
                        }, 5000);
                        $('.arrow').animate({
                          width: percentComplete * 100 + '%'
                        }, 3000);
                        console.log($('.arrow').width());
                  }
              }, false);
              return xhr;
          },
          type: 'POST',
          url: "api.php",
          success: function (data) {}
      });
    });
</script>
</body>
</html>
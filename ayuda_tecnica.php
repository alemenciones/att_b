<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="AT&T Business">
  <link rel="shortcut icon" type='image/x-icon' href="./public/img/favicon.png">
  <title>AT&T Business</title>
  <link rel="stylesheet" type="text/css" href="./src/css/_main.css" />
  <link rel="stylesheet" type="text/css" href="./src/css/logged.css" />
</head>

<body>
<!-- HEADER -->
<?php include './header.php';?>
<!-- END HEADER -->
<!-- CONTENT -->
<div id="main-body">
  <section id="logged-section" class="flex">
    <div id="menu">
      <div id="close-menu-mobile"><span>X</span></div>
      <div class="menu-col" id="resumen">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-resumen.png"><div class="menu-icon-title">RESUMEN</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen</a></div>
          <div class="menu-item-sub-item"><a href="#">Análisis</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-pagos.png"><div class="menu-icon-title">PAGOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen de Factura</a></div>
          <div class="menu-item-sub-item"><a href="#">Completar Pago</a></div>
          <div class="menu-item-sub-item"><a href="#">Historial de Pagos</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-mis_servicios.png"><div class="menu-icon-title">MIS SERVICIOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Servicios Activos</a></div>
          <div class="menu-item-sub-item"><a href="#">Ordenes</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-ayuda_tecnica.png"><div class="menu-icon-title">AYUDA TÉCNICA</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Nuevo Ticket</a></div>
          <div class="menu-item-sub-item"><a href="#">Resumen de tickets</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-perfil.png"><div class="menu-icon-title">PERFIL</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Manage Profile</a></div>
        </div>
      </div>

    </div>
    <div id="right">
      <div class="box border-blue p-20 mb-20">
        <div class="hd1">TICKET NUEVO</div>
        <div class="bold mb-10">Crear un ticket nuevo:</div>
        <div class="mb-10">
          <span class="bold">De:</span> <i class="mr-20">[pre-fill] Nombre del usuario</i><span class="bold">A:</span> <i>[pre-fill] dl-pr-noc@att.com</i>
        </div>
        <div class="mb-10">
          <span class="bold">Mensaje: </span> Escríbe aquí el detalle que necesitas.
        </div>
        <form>
          <div class="mb-10">
            <textarea class="input-text full-width" rows="10"></textarea>
          </div>
          <div class="mb-20"><center><button id="asdasd" class="btn-send2 bold">Enviar</button></center></div>
          <div class="bold mb-10">Puede crear un ticket nuevo, llamando al <i>787-717-9900</i> | tambíen puede visitar: <i><a target="__BLANK" href="attnocpr.com">www.attnocpr.com</a></i></div>
        </form>
      </div>
      <div class="box border-blue">
        <div class="hd1  pl-20 pt-20 pr-20 mb-0">Resumen de tickets</div>
        <div class="bold  pl-20 pb-20 pr-20">Este es un breve resumen de los tickets activos en su cuenta.</div>
        <div class="resumen-copy-box">1</div>
        <div class="resumen-copy-box">2</div>
        <div class="resumen-copy-box">3</div>
        <div class="resumen-copy-box">4</div>
      </div>



    </div>
  </section>
</div>
<!-- END CONTENT -->
<!-- FOOTER -->
<footer id="main-footer">
  <div class="footer-center">
    <div class="footer-top flex justify mb-40">
      <div>
        <div><a class="footer_options bold">Make a Payment</a></div>
        <div><a class="footer_options bold">Manage Profile</a></div>
        <div><a class="footer_options bold">Frequently Asked Questions</a></div>
      </div>
      <div>
        <div><a class="footer_options">Contact AT&T Business</a></div>
        <div><a class="footer_options">Help & Support</a></div>
      </div>
      <div>
        <div class="find_us">FIND US</div>
        <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
        <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
      <div class="align-center">
        <span><a href="#" class="footer_options">Términos de uso</a> | </span>
        <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
        <span><a href="#" class="footer_options">Accesibilidad</a></span>
      </div>
    </div>
  </div>
</footer>
<script src="./src/js/jquery-3.2.1.min.js"></script>
<script src="./src/js/App.js"></script>
<script>
  var request_content = `<div style="background-color: white">
                            <img src="./src/drawable/ticket_confirmation.png" width="20%" style="margin:10% 42% 3%;">
                              <div class="request-title" style="font-family: 'ATTAleckSlab-Regular'; font-size: 2em; text-align: center; margin-bottom: 3%;">Su ticket ha sido enviado con éxito</div>
                          </div>`;
  app.popUp('#asdasd', request_content);
</script>
</body>
</html>
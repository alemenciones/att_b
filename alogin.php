<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="AT&T Business">
  <link rel="shortcut icon" type='image/x-icon' href="./public/img/favicon.png">
  <title>AT&T Business</title>
  <link rel="stylesheet" type="text/css" href="./src/css/_main.css" />
  <link rel="stylesheet" type="text/css" href="./src/css/login.css" />
</head>

<body>
<!-- HEADER -->
<header id="main-header">
  <div class="main-header-center">
    <div class="logo">
      <img class="att-logo" src="./src/drawable/att-logo.png">
      <div class="att-logo-copy">AT&T Business</div>
    </div>
    <div class="main-header-language-box">
      <a href="#" class="main-header-language-box-option-active">Español</a>
      <a href="#" class="main-header-language-box-option">English</a>
    </div>
  </div>
</header>
<!-- END HEADER -->

<!-- CONTENT -->
<div id="main-body">
  <section id="login-section">
    <div class="center flex justify">
      <div class="box center-left p-20">
        <div class="login-left-copy1">Accede</div>
        <div class="login-left-copy2">A tu servicio de línea fija</div>

        <div>
          <form>
            <div class="mb-20">
              <label>Usuario:</label>
              <div>
                <input type="text" class="login-input" name="">
              </div>
            </div>
            <div class="mb-20">
              <label>Contraseña:</label>
              <div>
                <input type="text" class="login-input" name="">
              </div>
            </div>
          </form>
        </div>
      </div>


      <div class="box center-right p-20">
        <div class="login-left-copy1 mb-20">Este portal te da la oportunidad de ver y manejar las cuentas que tienes con AT&T</div>
        <div class="login-right-copy2 font-blue">Las transacciones disponibles en este portal son:</div>
        <div>
          <ul class="login-right-list font-blue">
            <li>Pagar tu factura</li>
            <li>Ver el resumen e historial de tu cuenta</li>
            <li>Ver tus facturas y estatus de órdenes</li>
            <li>Bajar archivos</li>
            <li>Contactarnos</li>
          </ul>
        </div>
      </div>


    </div>
  </section>
</div>
<!-- END CONTENT -->

<!-- FOOTER -->
<footer id="main-footer"></footer>
</body>

</html>
<header id="main-header">
  <div class="main-header-center">
    <div class="logo">
      <img class="att-logo" src="./src/drawable/att-logo.png">
      <div class="att-logo-copy"><span style="font-family: ATTAleckSans-Regular">AT&T</span> Business</div>
    </div>
    <div id="mobile-menu">
      <div class="mobile-menu-line"></div>
      <div class="mobile-menu-line"></div>
      <div class="mobile-menu-line"></div>
    </div>
    <div class="lang-desktop main-header-language-box">
      <a href="#" class="main-header-language-box-option-active">Español</a>
      <a href="#" class="main-header-language-box-option">English</a>
    </div>
  </div>
</header>
<div id="sub-menu">
  <div id="sub-menu-center">

    <div class="sub-menu-col user_name">
      <div class="font-large medium mt-10">Bienvenida, Karla</div>
      <div class="font-small"><span class="bold">Ultima visíta:</span> 10/13/2017 <span class="ml-10 font-blue">4:25pm</span></div>
    </div>

    <div class="sub-menu-col sub-menu-col-gray">
      <div class="sub-menu-2-left">
        <img class="sub-menu-2-left-img" src="./src/drawable/icon-profile.png">
        <div class="sub-menu-2-left-right ml-10">
          <div class="font-small">cuenta</div>
          <div id="desplegate-value" class=" font-blue font-large bold">000000-0</div>
        </div>
        <div class="sub-menu-2-left-desplegate">
          <div class="desplegate-item">000000-1</div>
          <div class="desplegate-item">000000-2</div>
          <div class="desplegate-item">000000-3</div>
          <div class="desplegate-item">000000-4</div>
          <div class="desplegate-item">000000-5</div>
        </div>
      </div>
      <div id="desplegate-sub-menu-icon" class="sub-menu-2-right"></div>
    </div>

    <div class="sub-menu-col sub-menu-col-gray">
      <div class="sub-menu-2-left-2">
        <img class="sub-menu-2-left-img" src="./src/drawable/icon-dollar.png">
        <div class="sub-menu-2-left-right ml-10 mr-10">
          <div class="font-small">Balance actual</div>
          <div class=" font-blue font-large bold">$853.82</div>
        </div>
      </div>
    </div>

    <a href="#" class="sub-menu-col complete-payment ml-30 bold">COMPLETAR UN PAGO</a>

    <div class="sub-menu-close-session">
      <a href="#">cerrar sesion</a> <img class="icon-logout" src="./src/drawable/icon-logout.png">
    </div>

  </div>
</div>
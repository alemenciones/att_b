function App () {
  var self = this;

  this.menu = function () {

    var menuCol = $(".menu-col");
    var menuItem = $('.menu-item');
    var menuItemContent = $('.menu-item-content');

    menuCol.on('click', function () {
      menuItemContent.fadeOut(100);
      menuItem.css({'background' : '#2aace3', 'width' : '100%', 'right' : '0%'});

      $(this).find('.menu-item-content').toggle();
      $(this).find('.menu-item').css({'background' : 'black', 'width' : '102%', 'right' : '-2%', 'box-shadow' : 'inset -4px 0 0 #2aace3'});
    })

  }

  this.despletateSubmenuAccount = function () {
    var subMenuLeft = $('.sub-menu-2-left, #desplegate-sub-menu-icon');
    var subMenuLeftDesplegate = $('.sub-menu-2-left-desplegate');
    var desplegateValue = $('#desplegate-value');
    var desplegateItem = $('.desplegate-item');
    
    subMenuLeft.on('click', function (ev) {
      ev.stopPropagation();
      subMenuLeftDesplegate.toggle();
    });

    desplegateItem.click(function (ev) {
      ev.preventDefault();
      desplegateValue.html($(this).html());
    });

  }

  this.despletateResumen = function () {
    var subMenuLeft = $('.resumen-box1-main-left, #desplegate-sub-menu-icon-2');
    var subMenuLeftDesplegate = $('.resumen-box1-main-left-desplegate');
    var desplegateValue1 = $('#resumen-value-1');
    var desplegateValue2 = $('#resumen-value-date-1');

    var desplegateItem = $('.resumen-desplegate-item');
    
    subMenuLeft.on('click', function (ev) {
      ev.stopPropagation();
      subMenuLeftDesplegate.toggle();
    });

    desplegateItem.click(function (ev) {
      ev.preventDefault();
      desplegateValue1.html($(this).find('.resumen-value-number').html());
      desplegateValue2.html($(this).find('.resumen-desplegate-item-right-copy').html());
    });

  }

  this.popUp = function (pointer, html) {
    var click = pointer;
    var copy = html;
    var body = $('body');
    var box = '<div class="bg-pop_up"><div class="box-pop_up">'+ copy +'<div class="close-pop_up-box"><div class="close-pop_up">X</div><div class="bold">CLOSE</div><div></div></div>';

    $(click).on('click', function (ev) {
      ev.preventDefault();
      body.prepend(box);
    });

    body.on('click', '.close-pop_up-box', function () {
      body.find('.bg-pop_up').remove();
    });
  }

  this.popUpServices = function (html) {
    var copy = html;
    var body = $('body');
    var box = '<div class="bg-pop_up"><div class="box-pop_up">'+ copy +'<div class="close-pop_up-box"><div class="close-pop_up">X</div><div class="bold">CLOSE</div><div></div></div>';
    body.prepend(box);

    body.on('click', '.close-pop_up-box', function () {
      body.find('.bg-pop_up').remove();
    });
  }

  this.mobileMenu = function () {

    var menu = $('#mobile-menu');
    var closeMenuMobile = $('#close-menu-mobile');

    menu.on('click', function () {
      $('#menu').fadeIn();
    });

    closeMenuMobile.on('click', function () {
      $('#menu').fadeOut();
    });

  }

}

var app = new App;
app.menu();
app.mobileMenu();
app.despletateSubmenuAccount();
app.despletateResumen();
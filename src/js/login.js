var login = {
    // muestra mensaje de error en el login de acuerdo al tipo de error
    // error: true si credenciales incorrectos, false si credenciales correctas
    // elements: arreglo de elementos html sobre los que se produce el error (input usurio, input password)
    // message: mensaje a mostrar
    validateLogin: function(error, elements, message){
        img = '<img class="msg_icon" src="src/drawable/warning_icon.png" width="1%">';
        msg_type = 'login_message';
        if(error){
            // adicionar icono de warning en el componente del error
            var warning_icon = '<img class="wrong_icon" src="src/drawable/wrong_icon.png" width="15px" height="15px">';
            if(elements.length > 0){
                for (var i = 0; i < elements.length; i++) {
                    $('#lbl_'+elements[i]).addClass('login_error'); // Cambiar color del label a rojo
                    $('#form_'+elements[i]).addClass('login_error'); // Cambiar color del borde a rojo
                    $('#form_'+elements[i]).css('margin-right', '3px'); // Adicionar margin right para mostrar icon warning
                    $('#input_error_'+elements[i]).append(warning_icon);
                }
            }
            // mostrar mensaje de requerimientos del password
            var password_requirements = '<div class="alert alert-danger password_requirements">'+
                                            '<strong>Tu contraseña!</strong><br/> debe tener mínimo 8 caracteres e incluir combinaciones de números y mayúscula.'+
                                        '</div>';
            // $('#password_requirements').html(password_requirements);
        }
        else {
            img = '<img class="msg_icon" src="src/drawable/ok_icon.png" width="1%">';
            msg_type = 'login_success';
            if(elements.length > 0){
                for (var i = 0; i < elements.length; i++) {
                    $('#lbl_'+elements[i]).removeClass('login_error'); // Quitar color del label a rojo
                    $('#form_'+elements[i]).removeClass('login_error'); // Quitar color del borde a rojo
                    $('#form_'+elements[i]).css('margin-right', '0px'); // Quitar margin right 
                }
            }
            $('.bg_message').html(''); // Eliminar mensaje de error debajo del header
            $( ".wrong_icon").remove(); // quitar icono de warning en el componente del error
            $('#password_requirements').html(''); // quitar mensaje de requerimientos del password
        }
        // mostrar mensaje de error/success debajo del header
        var msg = '<div class="'+msg_type+'"><div>'+img+message+'</div></div>';
        $('#message').fadeIn( "3000", function() {
            $('#message').html(msg);
        });
        var interval = setInterval(function () {
            $('#message').fadeOut( "3000", function() {
                $('#message').html('');
            });
            clearInterval(interval);
        }, 5000);
    }
}


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="ATT BUSINESS">
    <link type="text/css" rel="stylesheet" href="src/css/_main.css"/>
    <link type="text/css" rel="stylesheet" href="src/css/recovery_password.css"/>
    <link type="text/css" rel="stylesheet" href="src/css/bootstrap.min.css"/>
    <title>Att Business - Password Recovery</title>
</head>
<body>

    <!-- HEADER -->
    <header id="main-header">
      <div class="main-header-center">
        <div class="logo">
          <img class="att-logo" src="./src/drawable/att-logo.png">
          <div class="att-logo-copy"><span style="font-family: Arial">AT&T</span> Business</div>
        </div>
        <div class="main-header-language-box">
          <a href="#" class="main-header-language-box-option-active">Español</a>
          <a href="#" class="main-header-language-box-option">English</a>
        </div>
      </div>
    </header>
    <div class="profile-recovery-password-title">recuperar contraseña</div>
    <div class="profile-recovery-password-title1">No hay problema, solo necesitamos encontrar tu cuenta y confirmar tu identidad antes de continuar.</div>

    <div class="box recovery-password-box">
        <div class="recovery-password-title">paso 2</div>
        <div class="step1-active">
            <div class="step-box">
                <div class="circle"><div class="circle-text bold">1</div></div>
                <div class="circle circle-active"><div class="circle-active-text bold">2</div></div>
                <div class="circle"><div class="circle-text bold">3</div></div>
                <div class="step-horizontal-bar"></div>
            </div>
        </div>

        <div class="step2-content1">Escribe el código que recibiste en tu correo electrónico. <br/>Luego escribe tu nuevo código de acceso [password] y confírmalo.</div>
        <div class="step2-content2">
            <form>
                <div class="m-row">
                    <div class="m-row-left bold">Entrar código enviado por email:</div>
                    <div class="m-row-right"><input class="form-control input-sm" type="text" placeholder="Ex. johndoe@att.com"></div>
                </div>
                <div class="m-row">
                    <div class="m-row-left bold">Entrar contraseña:</div>
                    <div class="m-row-right"><input class="form-control input-sm" type="password"></div>
                </div>
                <div class="m-row">
                    <div class="m-row-left bold">Confirmar nueva contraseña:</div>
                    <div class="m-row-right"><input class="form-control input-sm" type="password"></div>
                </div>
            </form>
        </div>
        <div class="recovery-password-footer">
            <button class="recovery_password-footer-btn copy" id="btnStep2">continuar</button>
        </div>
    </div>

    <footer id="main-footer">
      <div class="footer-center">
        <div class="footer-top flex justify mb-40">
          <div>
            <div><a class="footer_options bold">Make a Payment</a></div>
            <div><a class="footer_options bold">Manage Profile</a></div>
            <div><a class="footer_options bold">Frequently Asked Questions</a></div>
          </div>
          <div>
            <div><a class="footer_options">Contact AT&T Business</a></div>
            <div><a class="footer_options">Help & Support</a></div>
          </div>
          <div>
            <div class="find_us">FIND US</div>
            <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
            <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
          </div>
        </div>
        <div class="footer-bottom">
          <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
          <div class="align-center">
            <span><a href="#" class="footer_options">Términos de uso</a> | </span>
            <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
            <span><a href="#" class="footer_options">Accesibilidad</a></span>
          </div>
        </div>
      </div>
    </footer>

    <script type="text/javascript" src="src/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
</body>
</html>
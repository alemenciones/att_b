<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="AT&T Business">
  <link rel="shortcut icon" type='image/x-icon' href="./public/img/favicon.png">
  <title>AT&T Business</title>
  <!-- <link rel="stylesheet" type="text/css" href="./src/css/logged.css"/> -->
  <link rel="stylesheet" type="text/css" href="./src/css/services.css" />
  <link rel="stylesheet" type="text/css" href="./src/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="./src/css/_main.css" />
</head>

<body>
<!-- HEADER -->
<?php include './header.php';?>
<!-- END HEADER -->
<!-- CONTENT -->
<div id="main-body">
  <section id="logged-section" class="flex">
    <div id="menu">
      <div id="close-menu-mobile"><span>X</span></div>
      <div class="menu-col" id="resumen">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-resumen.png"><div class="menu-icon-title">RESUMEN</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen</a></div>
          <div class="menu-item-sub-item"><a href="#">Análisis</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-pagos.png"><div class="menu-icon-title">PAGOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen de Factura</a></div>
          <div class="menu-item-sub-item"><a href="#">Completar Pago</a></div>
          <div class="menu-item-sub-item"><a href="#">Historial de Pagos</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-mis_servicios.png"><div class="menu-icon-title">MIS SERVICIOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Servicios Activos</a></div>
          <div class="menu-item-sub-item"><a href="#">Ordenes</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-ayuda_tecnica.png"><div class="menu-icon-title">AYUDA TÉCNICA</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Nuevo Ticket</a></div>
          <div class="menu-item-sub-item"><a href="#">Resumen de tickets</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-perfil.png"><div class="menu-icon-title">PERFIL</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Manage Profile</a></div>
        </div>
      </div>

    </div>
    <div id="right">
        <div class="box border-blue">
          <div class="services-title1">Servicios activos</div>
          <div class="services-title2">Mira un resumen personalizado de tu plan y factura.</div>
        </div>

        <div>
            <table class="table table-responsive table-hover">
                <thead>
                    <tr>
                        <th>Ordenes</th>
                        <th>Pagos</th>
                        <th>Descripcion</th>
                        <th>All history</th>
                    </tr>
                </thead>
                <tbody>
                    <tr  class="service-event-click">
                        <td>JAOK719</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                    <tr class="service-event-click">
                        <td>JAOK729</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                    <tr class="service-event-click">
                        <td>JAOK739</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                    <tr class="service-event-click">
                        <td>JAOK749</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                    <tr class="service-event-click">
                        <td>JAOK759</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="box border-blue">
          <div class="services-title1">Órdenes</div>
          <div class="services-title2">Revisa aquí tus órdenes activas.</div>
        </div>

        <div>
            <table class="table table-responsive table-hover">
                <thead>
                    <tr>
                        <th>Ordenes</th>
                        <th>Pagos</th>
                        <th>Descripcion</th>
                        <th>All history</th>
                    </tr>
                </thead>
                <tbody>
                    <tr  class="service-event-click">
                        <td>JAOK789</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                    <tr class="service-event-click">
                        <td>JAOK789</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                    <tr class="service-event-click">
                        <td>JAOK789</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                    <tr class="service-event-click">
                        <td>JAOK789</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                    <tr class="service-event-click">
                        <td>JAOK789</td>
                        <td>$50.00</td>
                        <td>Lorem ipsum dolor sit amet, consectetur ...</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
  </section>
</div>
<!-- END CONTENT -->
<!-- FOOTER -->
<footer id="main-footer">
  <div class="footer-center">
    <div class="footer-top flex justify mb-40">
      <div>
        <div><a class="footer_options bold">Make a Payment</a></div>
        <div><a class="footer_options bold">Manage Profile</a></div>
        <div><a class="footer_options bold">Frequently Asked Questions</a></div>
      </div>
      <div>
        <div><a class="footer_options">Contact AT&T Business</a></div>
        <div><a class="footer_options">Help & Support</a></div>
      </div>
      <div>
        <div class="find_us">FIND US</div>
        <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
        <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
      <div class="align-center">
        <span><a href="#" class="footer_options">Términos de uso</a> | </span>
        <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
        <span><a href="#" class="footer_options">Accesibilidad</a></span>
      </div>
    </div>
  </div>
</footer>
<script src="./src/js/jquery-3.2.1.min.js"></script>
<script src="./src/js/App.js"></script>
<script>
  $('.service-event-click').on('click', function () {
    app.popUpServices($(this).html());
  });
</script>
</body>
</html>
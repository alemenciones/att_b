<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="AT&T Business">
  <link rel="shortcut icon" type='image/x-icon' href="./public/img/favicon.png">
  <title>AT&T Business</title>
  <!-- <link rel="stylesheet" type="text/css" href="./src/css/logged.css"/> -->
  <link rel="stylesheet" type="text/css" href="./src/css/profile.css" />
  <link rel="stylesheet" type="text/css" href="./src/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="./src/css/_main.css" />
</head>

<body>
<!-- HEADER -->
<?php include './header.php';?>
<!-- END HEADER -->
<!-- CONTENT -->
<div id="main-body">
  <section id="logged-section" class="flex">
    <div id="menu">
      <div id="close-menu-mobile"><span>X</span></div>
      <div class="menu-col" id="resumen">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-resumen.png"><div class="menu-icon-title">RESUMEN</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen</a></div>
          <div class="menu-item-sub-item"><a href="#">Análisis</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-pagos.png"><div class="menu-icon-title">PAGOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Resumen de Factura</a></div>
          <div class="menu-item-sub-item"><a href="#">Completar Pago</a></div>
          <div class="menu-item-sub-item"><a href="#">Historial de Pagos</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-mis_servicios.png"><div class="menu-icon-title">MIS SERVICIOS</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Servicios Activos</a></div>
          <div class="menu-item-sub-item"><a href="#">Ordenes</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-ayuda_tecnica.png"><div class="menu-icon-title">AYUDA TÉCNICA</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Nuevo Ticket</a></div>
          <div class="menu-item-sub-item"><a href="#">Resumen de tickets</a></div>
        </div>
      </div>

      <div class="menu-col" id="pagos">
        <div class="menu-item"><img class="icon-menu-item" src="./src/drawable/icon-perfil.png"><div class="menu-icon-title">PERFIL</div></div>
        <div class="menu-item-content">
          <div class="menu-item-sub-item"><a href="#">Manage Profile</a></div>
        </div>
      </div>

    </div>
    <div id="right">
        <div class="box border-blue">
            <div class="profile-title">perfil</div>
            <div class="profile-data">
                <div class="input-component">
                    <label class="bold" for="name">Nombre:</label>
                    <input type="text" class="form-control medium input-text" id="name" name="name" value="Karla Márquez">
                </div>
                <div class="input-component">
                    <label class="bold" for="position">Posición:</label>
                    <input type="text" class="form-control medium input-text" id="position" name="position" value="Account Director">
                </div>
                <div class="input-component">
                    <label class="bold" for="dpto">Departamento:</label>
                    <input type="text" class="form-control medium input-text" id="dpto" name="dpto" value="Digital">
                </div>
                <div class="input-component">
                    <label class="bold" for="email">Email:</label>
                    <input type="email" class="form-control medium input-text" id="email" name="email" value="karla.marquez@jwt.com">
                </div>
            </div>
            <hr class="separator-line"/>
            <div class="profile-title">información de la compañía</div>
            <div class="profile-data">
                <div class="textarea-component">
                    <label class="bold">Dirección de facturación:</label>
                    <textarea class="form-control medium input-text" rows="3" id="comment" style="width: 100%">Jay Walter Thompson PO Bos 2125 San Juan, Puerto Rico 00922 2125</textarea>
                </div>
                <div class="textarea-component">
                    <label class="bold">Dirección física:</label>
                    <textarea class="form-control medium input-text" rows="3" id="comment" style="width: 100%">Jay Walter Thompson PO Bos 2125 San Juan, Puerto Rico 00922 2125</textarea>
                </div>
            </div>
        </div>
        <div class="box border-blue edit-profile-request">
            <div class="edit-profile-title bold">para editar tu perfil o la información de la compañía, necesita completar la solicitud y enviarla
            </div>
            <div class="profile-data">
                <div class="input-component">
                    <label class="bold from">De:</label><span class="from-to"> [pre-fill] Nombre del usuario</span>
                </div>
                <div class="input-component">
                    <label class="bold">A:</label><span class="from-to"> [pre-fill] dl-pr-noc@att.com</span>
                </div>
            </div>
            <div class="btn-complete-request text-center" id="complete_request">Completar solicitud</div>
            <div class="optional-message">
                <label class="bold">Mensaje opcional:</label><span class="complete-message"> Completa el encasillado</span>
                <textarea class="form-control medium input-text" rows="5" id="comment" style="width: 100%"></textarea>
            </div>
            <div class="edit-profile-footer">
                <div class="bold assistance">Para asistencia, llamar al 787-717-9700</div>
                <div class="btn-send-container">
                    <div class="text-center btn-send bold">enviar</div>
                </div>
            </div>
        </div>
         <!-- start recovery password step 1 -->
        <div class="box edit-profile-request" id="recovery_password1">
            <div class="profile-recovery-password-title">recuperar contraseña</div>
            <div class="profile-recovery-password-title1">No hay problema, solo necesitamos encontrar tu cuenta y confirmar tu identidad antes de continuar.</div>
            <div class="profile-recovery-password-title">paso 1</div>
            <div class="step-box">
                <div class="circle circle-active"><div class="circle-active-text bold">1</div></div>
                <div class="circle"><div class="circle-text bold">2</div></div>
                <div class="circle"><div class="circle-text bold">3</div></div>
                <div class="step-horizontal-bar"></div>
            </div>
            <div class="profile-recovery-password-text">Escribe tu correo electrónico. Recibirás un email con el código para que puedas proceder a recuperar tu contraseña.</div>
            <form>
              <input type="text" class="form-control input-email" placeholder="Ex. johndoe@att.com">
            </form>
            <div class="recovery_password-footer">
                <button class="recovery_password-footer-btn copy" id="btnStep1">continuar</button>
            </div>
        </div>
        <!-- end recovery password step 1 -->
        <!-- start recovery password step 2 -->
        <div class="box edit-profile-request" id="recovery_password2" style="display: none;">
            <div class="profile-recovery-password-title">recuperar contraseña</div>
            <div class="profile-recovery-password-title1">No hay problema, solo necesitamos encontrar tu cuenta y confirmar tu identidad antes de continuar.</div>
            <div class="profile-recovery-password-title">paso 2</div>
            <div class="step-box">
                <div class="circle"><div class="circle-text bold">1</div></div>
                <div class="circle circle-active"><div class="circle-active-text bold">2</div></div>
                <div class="circle"><div class="circle-text bold">3</div></div>
                <div class="step-horizontal-bar"></div>
            </div>
            <div class="profile-recovery-password-text">Escribe el código que recibiste en tu correo electrónico. Luego escribe tu nuevo código de acceso [password] y confírmalo.</div>
            <div class="step2-form-container">
                <form>
                    <div class="m-row">
                        <div class="m-row-left bold">Entrar código enviado por email:</div>
                        <div class="m-row-right"><input class="form-control" type="text" name=""></div>
                    </div>
                    <div class="m-row">
                        <div class="m-row-left bold">Entrar contraseña:</div>
                        <div class="m-row-right"><input class="form-control" type="password" name=""></div>
                    </div>
                    <div class="m-row">
                        <div class="m-row-left bold">Confirmar nueva contraseña:</div>
                        <div class="m-row-right"><input class="form-control" type="password" name=""></div>
                    </div>
                </form>
            </div>
            <div class="recovery_password-footer">
                <button class="recovery_password-footer-btn copy" id="btnStep2">continuar</button>
            </div>
        </div>
        <!-- end recovery password step 2 -->
        <!-- start recovery password step 3 -->
        <div class="box edit-profile-request" id="recovery_password3" style="display: none;">
            <div class="profile-recovery-password-title">recuperar contraseña</div>
            <div class="profile-recovery-password-title1">No hay problema, solo necesitamos encontrar tu cuenta y confirmar tu identidad antes de continuar.</div>
            <div class="profile-recovery-password-title">paso 3</div>
            <div class="step-box">
                <div class="circle"><div class="circle-text bold">1</div></div>
                <div class="circle"><div class="circle-text bold">2</div></div>
                <div class="circle circle-active"><div class="circle-active-text bold">3</div></div>
                <div class="step-horizontal-bar"></div>
            </div>
            <div class="profile-recovery-password-text">Gracias, tu contraseña ha sido guardada con éxito. Ahora puede acceder y manejar su cuenta.</div>
            <div class="recovery_password-footer">
                <button class="recovery_password-footer-btn copy" id="btnStep3">entrar</button>
            </div>
        </div>
        <!-- end recovery password step 3 -->
    </div>
  </section>
</div>
<!-- END CONTENT -->
<!-- FOOTER -->
<footer id="main-footer">
  <div class="footer-center">
    <div class="footer-top flex justify mb-40">
      <div>
        <div><a class="footer_options bold">Make a Payment</a></div>
        <div><a class="footer_options bold">Manage Profile</a></div>
        <div><a class="footer_options bold">Frequently Asked Questions</a></div>
      </div>
      <div>
        <div><a class="footer_options">Contact AT&T Business</a></div>
        <div><a class="footer_options">Help & Support</a></div>
      </div>
      <div>
        <div class="find_us">FIND US</div>
        <a href=""><img src="src/drawable/fb_logo.png" width="30"></a>
        <a href=""><img src="src/drawable/ins_logo.png" width="30"></a>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="align-center mb-10">@2017. All rights reserved. AT&T Puerto Rico.</div>
      <div class="align-center">
        <span><a href="#" class="footer_options">Términos de uso</a> | </span>
        <span><a href="#" class="footer_options">Política de Privacidad</a> | </span>
        <span><a href="#" class="footer_options">Accesibilidad</a></span>
      </div>
    </div>
  </div>
</footer>
<script src="./src/js/jquery-3.2.1.min.js"></script>
<script src="./src/js/App.js"></script>
<script>
    $(document).ready(function(){
        $('#btnStep1').click(function(){
            $('#recovery_password1').hide();
            $('#recovery_password2').show();
        })
        $('#btnStep2').click(function(){
            $('#recovery_password2').hide();
            $('#recovery_password3').show();
        })
        var request_content = `<div class="request">
                                  <div class="request-title">Request via email to customer service</div>
                                  <div class="box-request-container">
                                      <form>
                                          <div class="m-row">
                                              <div class="box-request-left bold">To:</div>
                                              <div class="box-request-right"><input class="form-control" type="text" name=""></div>
                                          </div>
                                          <div class="m-row">
                                              <div class="box-request-left bold">From:</div>
                                              <div class="box-request-right"><input class="form-control" type="text" name=""></div>
                                          </div>
                                          <div class="m-row">
                                              <div class="box-request-left bold">Message:</div>
                                              <div class="box-request-right"><textarea class="form-control"></textarea></div>
                                          </div>
                                          <button class="submit-btn bold" id="btnStep1">submit</button>
                                          <div class="call_to text-center bold">Or call: 787-717-9700</div>
                                      </form>
                                  </div>
                              </div>`;
        app.popUp('#complete_request', request_content);
    });
</script>
</body>
</html>